package eu.hywse.addons.dustmc.server;

import com.google.gson.JsonObject;
import net.labymod.api.events.TabListEvent;
import net.labymod.core.LabyModCore;
import net.labymod.ingamegui.moduletypes.ColoredTextModule;
import net.labymod.main.LabyMod;
import net.labymod.servermanager.ChatDisplayAction;
import net.labymod.servermanager.Server;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;
import net.labymod.utils.ModColor;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

public class DustMCServer extends Server {

    public DustMCServer() {
        super("dustmc", new String[] {"dustmc.de", "dustmc.net", "dustmc.com"});
    }

    // Settings
    private boolean displayNick;
    private boolean displayBlockPercent;

    // Vars
    private String _nickname;
    private double _blockPercent;
    private Field currentBlockDamageField;
    private Field blockHitDelayField;
    private long stoppedBedLong = 0L;
    private int stoppedBedPercent = 0;

    public void onJoin(bde serverProfile) {

    }

    /*
     * Read / Draw
     */
    @Override
    public ChatDisplayAction handleChatMessage(String clean, String formatted) throws Exception {

        try {

            if(clean.length() <= 1) return ChatDisplayAction.NORMAL;
            String prefix = (clean.contains(" ") ? clean.split(" ")[0] : clean);

            // DMC-Info
            if(prefix.equalsIgnoreCase("[i]")) {

                // Display Nick
                if(clean.startsWith("[i] Du bist genickt und wirst") && clean.endsWith("heißen!")) {
                    _nickname = clean.split(" ")[6];
                }
                if(clean.equalsIgnoreCase("[i] Du bist anonym genickt!") || clean.equalsIgnoreCase("[i] Dein Nickname wird versteckt!")) {
                    _nickname = "{Anonym}";
                }
                if(clean.contains("Du bist nicht mehr genickt") || clean.contains("Du besitzt nun keinen Nicknamen mehr!")) {
                    _nickname = null;
                }

            }


        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return ChatDisplayAction.NORMAL;
    }

    /*
     * Display Values
     */
    @Override
    public void addModuleLines(List<Server.DisplayLine> lines) {
        super.addModuleLines(lines);

        try {

            // Nick
            if(this._nickname != null && this._nickname.length() > 0 && displayNick) {
                lines.add(new DisplayLine("Nick", Collections.singletonList(ColoredTextModule.Text.getText(_nickname))));
            }

        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /*
     * Display Respawnblock percentage
     */
    @Override
    public void draw() {
        if (this.displayBlockPercent) {

            try {
                boolean isBedBlock = false;
                zw targetBlockItem = LabyModCore.getMinecraft().getTargetBlockItem();
                isBedBlock = (targetBlockItem != null) && (targetBlockItem == zw.b(57));

                float currentBlockDamageValue = 0.0F;
                int blockHitDelay = 0;

                if ((isBedBlock && ((currentBlockDamageValue = getCurrentBlockDamageValue(ave.A().c)) != 0.0F))) {
                    this.stoppedBedPercent = ((int)(currentBlockDamageValue * 100.0F));
                    this.stoppedBedLong = (System.currentTimeMillis() + 3000L);
                }

                if (this.stoppedBedLong > System.currentTimeMillis())
                {
                    blockHitDelay = getBlockHitDelay(ave.A().c);
                    if (blockHitDelay != 0) {
                        this.stoppedBedPercent = 100;
                    }
                    if (this.stoppedBedPercent != 0) {
                        LabyMod.getInstance().getDrawUtils().drawString(ModColor.cl("a") + this.stoppedBedPercent + "% ", LabyMod.getInstance().getDrawUtils().getWidth() / 2 + 3,
                                LabyMod.getInstance().getDrawUtils().getHeight() / 2 - 10, 1.5D);
                    }
                } else {
                    this.stoppedBedPercent = 0;
                    if (isBedBlock && ((blockHitDelay = getBlockHitDelay(ave.A().c)) != 0)) {
                        LabyMod.getInstance().getDrawUtils().drawString(ModColor.cl("c") + blockHitDelay, LabyMod.getInstance().getDrawUtils().getWidth() / 2 + 1,
                                LabyMod.getInstance().getDrawUtils().getHeight() / 2 - 10, 1.5D);
                    }
                }
            } catch (Exception error) {
                error.printStackTrace();
            }

        }
    }

    /**
     * @param playerControllerMP
     * @return Block hit delay
     */
    private int getBlockHitDelay(bda playerControllerMP)
    {
        if (this.blockHitDelayField == null) {
            this.blockHitDelayField = ReflectionHelper.findField(bda.class, LabyModCore.getMappingAdapter().getBlockHitDelayMappings());
        }
        try
        {
            return ((Integer)this.blockHitDelayField.get(playerControllerMP)).intValue();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * @param playerControllerMP
     * @return Block damage
     */
    private float getCurrentBlockDamageValue(bda playerControllerMP)
    {
        if (this.currentBlockDamageField == null) {
            this.currentBlockDamageField = ReflectionHelper.findField(bda.class, LabyModCore.getMappingAdapter().getCurBlockDamageMpMappings());
        }

        try {
            return ((Float)this.currentBlockDamageField.get(playerControllerMP)).floatValue();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return 0.0F;
    }

    @Override
    public void handlePluginMessage(String s, em em) { }

    @Override
    public void handleTabInfoMessage(TabListEvent.Type type, String s, String s1) throws Exception { }


    /*
     * load Settings
     */
    @Override
    protected void initConfig(JsonObject object) {
        super.initConfig(object);

        this.displayNick = getBooleanAttribute("displayNick", true);
        this.displayBlockPercent = getBooleanAttribute("displayBedPercentage", true);
    }

    /*
     * Add Settings
     */
    @Override
    public void fillSubSettings(List<SettingsElement> list) {
        // Display Nick
        list.add(
                new BooleanElement("Display Nick",
                        this,
                        new ControlElement.IconData(Material.NAME_TAG),
                        "displayNick"));

        // Display Bed-Destruction
        list.add(
                new BooleanElement("Display Bed-Destruction",
                        this,
                        new ControlElement.IconData(Material.BED),
                        "displayBedPercentage"));
    }

}
