package eu.hywse.addons.dustmc;

// import eu.hywse.addons.dustmc.server.DustMCServer;
import eu.hywse.addons.dustmc.server.DustMCServer;
import net.labymod.api.LabyModAddon;
import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.ModuleCategoryRegistry;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;

import java.util.List;

public class Main extends LabyModAddon {

    public static ModuleCategory DUSTMC_CATEGORY;
    public static DustMCServer server;

    @Override
    public void onEnable() {
        System.out.println("Addon geladen!");

        ModuleCategoryRegistry.loadCategory(DUSTMC_CATEGORY = new ModuleCategory("DustMC", true, new ControlElement.IconData("dustmc/textures/icon.png")));
        api.registerServerSupport(this, server = new DustMCServer());
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void loadConfig() {
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
    }
}
